package com.devcamp.midtest2307s20.stringapi;
import java.util.HashSet;
import java.util.Set;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

 @RestController
 @CrossOrigin
public class Controller {
    @GetMapping("/reverse") //1. Nhận đầu vào là một chuỗi bất kỳ từ request và trả ra một chuỗi bị đảo ngược
    public String reverseString(@RequestParam String string) {
      return new StringBuilder(string).reverse().toString();
    }

    @GetMapping("/isPalindrom") //2. Nhận đầu vào là một chuỗi bất kỳ từ request và trả ra message kiểm tra chuỗi có phải là palindrome không
    public static boolean isPalindrom(@RequestParam char[] word){
      int i1 = 0;
      int i2 = word.length - 1;
      while (i2 > i1) {
          if (word[i1] != word[i2]) {
              return false;
          }
          ++i1;
          --i2;
      }
      return true;
  }

    @GetMapping("replaceDuplicate")//3.Nhận đầu vào là một chuỗi bất kỳ từ request, thực hiện xóa các ký tự xuất hiện nhiều hơn một lần trong chuỗi, chỉ giữ lại ký tự đầu tiên và trả ra kết quả, ví dụ bananas => bans.
    public String replaceDup(@RequestParam String string) {
        Set<Character> charsPresent = new HashSet<>();
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < string.length(); i++) {
            if (!charsPresent.contains(string.charAt(i))) {
                stringBuilder.append(string.charAt(i));
                charsPresent.add(string.charAt(i));
            }
        }
        return stringBuilder.toString();
    }

    @GetMapping("healingString") //4.Nhận đầu vào là 2 tham số là chuỗi bất kỳ từ request, thực hiện gắn chúng lại với nhau, nếu 2 chuỗi có độ dài không bằng nhau thì tiến hành cắt bỏ các ký tự đầu của string dài hơn cho đến khi chúng bằng nhau thì tiến hành gắn lại. Sau đó trả ra kết quả. Ví dụ Welcome và home => comehome.
    public String healingString(@RequestParam String st1,String st2){
      {
        if (st1.length() == st2.length())
            return st1+st2;
        if (st1.length() > st2.length())
        {
            int diff = st1.length() - st2.length();
            return st1.substring(diff, st1.length()) + st2;
        } else
        {
            int diff = st2.length() - st1.length();
            return st1 + st2.substring(diff, st2.length());
        }
    }
  }
}
